package com.example.belajarspringboot.controllers;

import com.example.belajarspringboot.models.DTO.UserLoginReqDTO;
import com.example.belajarspringboot.models.User;
import com.example.belajarspringboot.services.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@CrossOrigin
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@Valid @RequestBody User user) {
        return new ResponseEntity<>(userService.register(user), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestHeader("login-id") String username, @Valid @RequestBody UserLoginReqDTO user) {
        return ResponseEntity.ok(userService.login(username, user));
    }

}
